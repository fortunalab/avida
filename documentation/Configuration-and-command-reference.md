## Main Configuration Files
* [[Avida.cfg|Avida.cfg]]
* [Events File](https://github.com/devosoft/avida/wiki/Events-file)
* [Environment File](https://github.com/devosoft/avida/wiki/Environment-file)
* [Instruction Set](https://github.com/devosoft/avida/wiki/Instruction-Set)
* [[instset-heads.cfg]]
* [[default-heads.org]]
* [Analyze File](https://github.com/devosoft/avida/wiki/Analyze-File)

## Analyze Mode
* [[Analyze File]]
* [[Sample Analyze Programs]]

## Subpopulations (Demes)
* [[Deme introduction]]
* [[Deme migration]]

## Misc. Configuration 
* [[Quirks]]
* [[Setting up Fixed-Length Organisms]]
* [[Introduction to Parasites]]
* [[Gradient Resources]]
* [[Internal resources]]
* [[Using mating types (separate sexes)]]
* [[Energy model configuration]]
