Avida is a free, open source software platform for conducting and analyzing experiments with self-replicating and evolving computer programs. It provides detailed control over experimental settings and protocols, a large array of measurement tools, and sophisticated methods to analyze and post-process experimental data.

The following articles provide a good introduction to Avida:
* [Testing Darwin](https://carlzimmer.com/testing-darwin-511/) by Carl Zimmer (Cover story of Feb 2005 Discover Magazine).
* [Twice as Natural](http://myxo.css.msu.edu/lenski/pdf/2001,%20Nature,%20Lenski,%20twice%20as%20natural.pdf) by Richard Lenski (2001)
* [Meet the Scientist](http://www.microbeworld.org/index.php?option=com_content&view=article&id=784:mts59-charles-ofria-&catid=37:meet-the-scientist&Itemid=1550) podcast interview of Charles Ofria by Carl Zimmer (2010)

More detailed materials:
* [About Digital Evolution](About.md)
* [Avida: A Software Platform for Research in Computational Evolutionary Biology](http://www.cse.msu.edu/~ofria/pubs/2009AvidaIntro.pdf) by C. Ofria, D.M. Bryson, and C. Wilke (2009).
* The contents of this Wiki

If you would like to contribute to this documentation, please create an account via the log-in link at the top-right of the page.

**[New User Information](New-User-Information.md)**

## General Information
* [Beginner Documentation](Beginner-Documentation.md)
* [Overview](Overview.md)
* [A Guided Tour of the Ancestor and its Hardware](Default-Ancestor-Guided-Tour.md)
* [Text Viewer](Text-Viewer.md)
* [Configuration and Command Reference](Configuration-and-Command-Reference.md)
* [Experiment and Analysis Guides](Experiment-and-Analysis-Guides.md)


## Developer Resources
* [Developers Guide](Developers-Guide.md)
* [Getting Started](Development-|-Getting-Started.md)
* [Avida 3 API](Avida-3-API.md)